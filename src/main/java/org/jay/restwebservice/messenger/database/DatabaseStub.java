package org.jay.restwebservice.messenger.database;

import java.util.HashMap;
import java.util.Map;

import org.jay.restwebservice.messenger.model.Message;
import org.jay.restwebservice.messenger.model.Profile;

public class DatabaseStub {

	private static Map<Long, Message> messages = new HashMap<>();
	private static Map<String, Profile> profiles = new HashMap<>();

	public static Map<Long, Message> getMessages() {
		return messages;
	}

	public static Map<String, Profile> getProfiles() {
		return profiles;
	}

}
