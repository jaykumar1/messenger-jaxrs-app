package org.jay.restwebservice.messenger.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jay.restwebservice.messenger.model.Message;
import org.jay.restwebservice.messenger.service.MessageService;

import com.sun.org.apache.xerces.internal.util.Status;

import apple.laf.JRSUIConstants.Size;

@Path("/messages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MessageResource {
	MessageService messageService = new MessageService();

	@GET
	public List<Message> getAllMessage(@QueryParam("year") int year, @QueryParam("start") int start,
			@QueryParam("stop") int size) {
		if (year > 0)
			return messageService.getAllMessagesForYear(year);
		if (start > 0 && size > 0)
			return messageService.getAllMessagesPaginated(start, size);
		return messageService.getAllMessages();
	}

	@GET
	@Path("/{messageId}")
	public Message getMessage(@PathParam("messageId") Long id, @Context UriInfo uriInfo) {
		Message message=messageService.getMessage(id);
		getUrlForSelf(id, uriInfo, message);
		getUrlForProfile(id, uriInfo, message);
		getUrlForComments(id, uriInfo, message);
		return message;
		
	}

	private void getUrlForComments(Long id, UriInfo uriInfo, Message message) {
		String url=uriInfo.getBaseUriBuilder()
				.path(MessageResource.class)
				.path(MessageResource.class, "getCommentResource")
				.resolveTemplate("messageId", message.getId())
				.build().toString();
		message.addLink(url, "comments");
		
	}

	private void getUrlForProfile(Long id, UriInfo uriInfo, Message message) {
		String url=uriInfo.getBaseUriBuilder()
				.path(ProfileResource.class)
				.path(message.getAuthor())
				.toString();
		message.addLink(url, "Profile");
		
	}

	private void getUrlForSelf(Long id, UriInfo uriInfo, Message message) {
		String url=uriInfo.getBaseUriBuilder().
				path(MessageResource.class)
				.path(Long.toString(id))
				.build().toString();
		message.addLink(url , "self");
	}

	@POST
	public Response addMessages(Message message, @Context UriInfo uriInfo)  {
		Message newMessage = messageService.addMessage(message);
		String newId=String.valueOf(newMessage.getId());
		URI uri=uriInfo.getAbsolutePathBuilder().path(newId).build();
		
		return Response.created(uri).entity(newMessage).build();
	}

	@PUT
	@Path("/{messageId}")
	public Message updateMessage(@PathParam("messageId") long id, Message message) {
		message.setId(id);
		return messageService.updateMessage(message);
	}

	@DELETE
	@Path("/{messageId}")
	public Message removeMessage(@PathParam("messageId") Long id) {
		return messageService.removeMessage(id);
	}

	@Path("/{messageId}/comments")
	public CommentResource getCommentResource() {
		return new CommentResource();
	}

}
