package org.jay.restwebservice.messenger.resource;

import javax.ws.rs.CookieParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;

public class InjectDemoFilterBean {
	private @MatrixParam("mParam") String mParam;
	private @HeaderParam("hParam") String hParam;
	private @CookieParam("cValue") String cValue;
	public String getmParam() {
		return mParam;
	}
	public void setmParam(String mParam) {
		this.mParam = mParam;
	}
	public String gethParam() {
		return hParam;
	}
	public void sethParam(String hParam) {
		this.hParam = hParam;
	}
	public String getcValue() {
		return cValue;
	}
	public void setcValue(String cValue) {
		this.cValue = cValue;
	}
	
	
}
