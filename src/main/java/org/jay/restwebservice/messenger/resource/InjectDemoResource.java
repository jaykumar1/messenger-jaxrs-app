package org.jay.restwebservice.messenger.resource;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/injection")
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.TEXT_PLAIN)
public class InjectDemoResource {

	@GET
	@Path("annotations")
	public String GetParamsUsingAnnotations(@BeanParam InjectDemoFilterBean IDFilterBean) {
		return "matrix Param " + IDFilterBean.getmParam() + " header Param " + IDFilterBean.gethParam()
				+ " cookies value " + IDFilterBean.getcValue();
	}

	@GET
	@Path("/context")
	public String getParamUsingContext(@Context UriInfo info) {
		String uri = info.getAbsolutePath().toString();
		return "path " + uri;
	}
}
