package org.jay.restwebservice.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.jay.restwebservice.messenger.database.DatabaseStub;
import org.jay.restwebservice.messenger.exception.DataNotFoundException;
import org.jay.restwebservice.messenger.model.Message;

public class MessageService {

	private static Map<Long, Message> messages = DatabaseStub.getMessages();

	public MessageService() {
		Message m1 = new Message(1l, "Hello World!", "O'Really");
		Message m2 = new Message(2l, "Hi there!", "Bob");
		messages.put(m1.getId(), m1);
		messages.put(m2.getId(), m2);
	}

	public List<Message> getAllMessagesForYear(int year) {
		List<Message> messageoftheyear = new ArrayList();
		Calendar cal = Calendar.getInstance();
		for (Message message : messages.values()) {
			cal.setTime(message.getCreated()); 
			if (cal.get(Calendar.YEAR) == year) {
				messageoftheyear.add(message);
			}
		}
		return messageoftheyear;
	}

	public List<Message> getAllMessagesPaginated(int start, int size) {
		ArrayList<Message> list = new ArrayList<>(messages.values());
		if (start + size > list.size())
			return new ArrayList<Message>();
		return list.subList(start, start + size);
	}

	public List<Message> getAllMessages() {
		return new ArrayList<Message>(messages.values());
	}

	public Message getMessage(Long id) {
		Message message = messages.get(id);
		if (message == null) {
			throw new DataNotFoundException("Data not found for ID" + id);
		}
		return messages.get(id);
	}

	public Message addMessage(Message message) {
		message.setId(messages.size() + 1);
		messages.put(message.getId(), message);
		return message;
	}

	public Message updateMessage(Message message) {
		if (message.getId() <= 0) {
			return null;
		}
		messages.put(message.getId(), message);
		return message;
	}

	public Message removeMessage(Long id) {
		return messages.remove(id);

	}

}
