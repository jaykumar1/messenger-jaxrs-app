package org.jay.restwebservice.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jay.restwebservice.messenger.database.DatabaseStub;
import org.jay.restwebservice.messenger.model.Comment;
import org.jay.restwebservice.messenger.model.ExceptionMessage;
import org.jay.restwebservice.messenger.model.Message;

public class CommentService {

	private Map<Long, Message> messages = DatabaseStub.getMessages();
	private Map<Long, Comment> comments = messages.get(1l).getComments();

	public CommentService() {
		Comment c1 = new Comment(1l, "Comment 1", "Author 1");
		Comment c2 = new Comment(2l, "Comment 2", "Author 2");
		comments.put(c1.getId(), c1);
		comments.put(c2.getId(), c2);
	}

	public List<Comment> getAllComments(long messageId) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		return new ArrayList<Comment>(comments.values());
	}

	public Comment getComment(long messageId, long commentId) {
		ExceptionMessage msz=new ExceptionMessage(404,"NOT FOUND","http://localhost:8080/messages/webapi");
		Response response=Response.status(Status.NOT_FOUND).entity(msz).build();
		Message message = messages.get(messageId);
		if (message == null) {
			throw new WebApplicationException(response);
		}
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		Comment comment = comments.get(commentId);
		if (comment == null) {
			throw new WebApplicationException(response);
		}
		return comment;
	}

	public Comment addComment(long messageId, Comment comment) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		comment.setId(comments.size() + 1);
		comments.put(comment.getId(), comment);
		return comment;
	}

	public Comment updateComment(long messageId, long commentId, Comment comment) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		comments.put(commentId, comment);
		return comment;
	}

	public Comment deleteComment(long messageId, long commentId) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		return comments.remove(commentId);

	}

}
