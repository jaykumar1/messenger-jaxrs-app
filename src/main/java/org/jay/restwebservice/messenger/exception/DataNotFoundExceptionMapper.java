package org.jay.restwebservice.messenger.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jay.restwebservice.messenger.model.ExceptionMessage;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException>{

	@Override
	public Response toResponse(DataNotFoundException ex) {
		ExceptionMessage message=new ExceptionMessage(404, ex.getMessage(), "No Data for this id "); 
		return Response.status(Status.NOT_FOUND).entity(message).build();
	}
	

}
