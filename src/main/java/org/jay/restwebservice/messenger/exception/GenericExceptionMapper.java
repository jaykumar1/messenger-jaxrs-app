package org.jay.restwebservice.messenger.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jay.restwebservice.messenger.model.ExceptionMessage;

public class GenericExceptionMapper implements ExceptionMapper<Throwable>{

	@Override
	public Response toResponse(Throwable ex) {
		ExceptionMessage exceptionMessage=new ExceptionMessage(500, ex.getMessage(), "http://localhost:8080/messages/webapi");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(exceptionMessage).build();
	}
	
	

}
