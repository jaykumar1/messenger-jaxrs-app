package org.jay.restwebservice.messenger.exception;

public class DataNotFoundException extends RuntimeException{
	
	
	private static final long serialVersionUID = 7033303854206740409L;

	public DataNotFoundException(String message){
		super(message);
	}

}
